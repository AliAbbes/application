from lbconfig.api import *
#-----------------------------------------------------------------------------#
# Configuration
#-----------------------------------------------------------------------------#
name='training'
name_ws='data_node'
lbconfig_package(
 name=name,
 version='0.1', 
 default_prefix='out',
 default_targets=['lb-libraries', 'archive-ws-data_node']
)
depends_on( 
 logicblox_dep, 
 lb_web_dep,
 measureBlox = {
 'default_path' : '$(LB_MEASURE_SERVICE_HOME)',
 'help' : 'The BloxWeb Measure Service.'
 }
)
target('workspaces', ['archive-ws-data_node'  ])
#-----------------------------------------------------------------------------#
# LogiQl libraries
#-----------------------------------------------------------------------------#
measure_modules = [
 'measure_dims',
 'measure_metrics'
]
core_modules = [
 'dims',
 'metrics'
]
services_modules = [
 'dims_tdx',
 'metrics_tdx'
]
ws_modules = [
 'data_node'
]
#-----------------------------------------------------------------------------#
# Dependency configuration that you should not have to mess with
#-----------------------------------------------------------------------------#
external_libs = {
 'lb_web' : '$(lb_web)',
 'lb_measure_service' : '$(measureBlox)/share/lb-measure-service',
 'lb_web_connectblox' : '$(lb_web)',
 'lb_measure_txn_proto': '$(measureBlox)/share/lb-measure-service'
}
#--
#-----------------------------------------------------------------------------#
# compiling LogiQl libraries
#-----------------------------------------------------------------------------#
for lib_name in core_modules:
 lb_library( name=lib_name, srcdir='src/core', deps=external_libs )
for lib_name in services_modules:
 lb_library( name=lib_name, srcdir='src/services/tdx_services', deps=external_libs )
for lib_name in ws_modules:
 lb_library( name=lib_name, srcdir='src', deps=external_libs )
for lib_name in measure_modules:
 lb_library( name=lib_name, srcdir='src/services', deps=external_libs )
#-----------------------------------------------------------------------------#
# Workspace
#-----------------------------------------------------------------------------#
ws_archive(name="data_node", libraries=['data_node'])
#-----------------------------------------------------------------------------#
# Install
#-----------------------------------------------------------------------------#
install_dir('$(build)/workspaces', 'workspaces')
#-----------------------------------------------------------------------------#
# Deploy
#-----------------------------------------------------------------------------#

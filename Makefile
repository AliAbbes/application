logicblox = /home/aliabbes/Documents/training/upstream/logicblox/project
lb_web = /home/aliabbes/Documents/training/upstream/logicblox/project
measureBlox = $(LB_MEASURE_SERVICE_HOME)
LB_DEPLOYMENT_HOME = /home/aliabbes/lb_deployment
version = 0.1
package_basename = training
package_name = training-$(version)
help = 0
file = config.py
extension = None
prefix = out
SHELL = /usr/bin/env bash
lb = /home/aliabbes/Documents/training/upstream/logicblox/project/bin/lb
V = 0
Q0 = @
Q1 = 
Q = $(Q$(V))
topdir = $(shell pwd)
build = $(topdir)/build
protoc = protoc
proto2datalog = $(logicblox)/bin/proto2datalog
testcase = 
all : \
	lb-libraries \
	archive-ws-data_node

$(build)/check_workspace_ws-data_node.success : \
	$(build)/sepcomp/data_node/LB_SUMMARY.lbp

	@echo Building workspace data_node >&2
	$(Q)rm -f $(build)/check_workspace_ws-data_node.success&&$(lb) create data_node --overwrite&&$(lb) addproject data_node $(build)/sepcomp/data_node --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --commit-mode diskcommit&&mkdir -p $(build) && touch $(build)/check_workspace_ws-data_node.success

$(build)/sepcomp/data_node : \
	$(build)/sepcomp/data_node/LB_SUMMARY.lbp

$(build)/sepcomp/data_node/LB_SUMMARY.lbp : \
	src/data_node.project \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp \
	$(build)/sepcomp/metrics/LB_SUMMARY.lbp \
	$(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp \
	$(build)/sepcomp/measure_dims/LB_SUMMARY.lbp \
	$(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp \
	$(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp

	@echo Compiling logicblox project data_node >&2
	$(Q)mkdir -p $(build)/sepcomp/data_node
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/data_node src/data_node.project' || ( rm -f $(build)/sepcomp/data_node/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/data_node/LB_SUMMARY.lbp

$(build)/sepcomp/dims : \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp

$(build)/sepcomp/dims/LB_SUMMARY.lbp : \
	src/core/dims.project \
	src/core/dims/calendar.logic \
	src/core/dims/location.logic \
	src/core/dims/product.logic

	@echo Compiling logicblox project dims >&2
	$(Q)mkdir -p $(build)/sepcomp/dims
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/dims src/core/dims.project' || ( rm -f $(build)/sepcomp/dims/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/dims/LB_SUMMARY.lbp

$(build)/sepcomp/dims_tdx : \
	$(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp

$(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp : \
	src/services/tdx_services/dims_tdx.project \
	src/services/tdx_services/dims_tdx/calendar.logic \
	src/services/tdx_services/dims_tdx/location.logic \
	src/services/tdx_services/dims_tdx/product.logic \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp

	@echo Compiling logicblox project dims_tdx >&2
	$(Q)mkdir -p $(build)/sepcomp/dims_tdx
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/dims_tdx src/services/tdx_services/dims_tdx.project' || ( rm -f $(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp

$(build)/sepcomp/measure_dims : \
	$(build)/sepcomp/measure_dims/LB_SUMMARY.lbp

$(build)/sepcomp/measure_dims/LB_SUMMARY.lbp : \
	src/services/measure_dims.project \
	src/services/measure_dims/calendar.logic \
	src/services/measure_dims/location.logic \
	src/services/measure_dims/product.logic \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp

	@echo Compiling logicblox project measure_dims >&2
	$(Q)mkdir -p $(build)/sepcomp/measure_dims
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/measure_dims src/services/measure_dims.project' || ( rm -f $(build)/sepcomp/measure_dims/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/measure_dims/LB_SUMMARY.lbp

$(build)/sepcomp/measure_metrics : \
	$(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp

$(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp : \
	src/services/measure_metrics.project \
	src/services/measure_metrics/metrics.logic \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp \
	$(build)/sepcomp/metrics/LB_SUMMARY.lbp

	@echo Compiling logicblox project measure_metrics >&2
	$(Q)mkdir -p $(build)/sepcomp/measure_metrics
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/measure_metrics src/services/measure_metrics.project' || ( rm -f $(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp

$(build)/sepcomp/metrics : \
	$(build)/sepcomp/metrics/LB_SUMMARY.lbp

$(build)/sepcomp/metrics/LB_SUMMARY.lbp : \
	src/core/metrics.project \
	src/core/metrics/sales.logic \
	src/core/metrics/forecast.logic \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp

	@echo Compiling logicblox project metrics >&2
	$(Q)mkdir -p $(build)/sepcomp/metrics
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/metrics src/core/metrics.project' || ( rm -f $(build)/sepcomp/metrics/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/metrics/LB_SUMMARY.lbp

$(build)/sepcomp/metrics_tdx : \
	$(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp

$(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp : \
	src/services/tdx_services/metrics_tdx.project \
	src/services/tdx_services/metrics_tdx/sales.logic \
	src/services/tdx_services/metrics_tdx/forecast.logic \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp \
	$(build)/sepcomp/metrics/LB_SUMMARY.lbp

	@echo Compiling logicblox project metrics_tdx >&2
	$(Q)mkdir -p $(build)/sepcomp/metrics_tdx
	$(Q)LOGICBLOX_HOME=$(logicblox)  sh -c '$(logicblox)/bin/lb compile project --libpath $(measureBlox)/share/lb-measure-service:$(lb_web):$(build)/sepcomp --out-dir $(build)/sepcomp/metrics_tdx src/services/tdx_services/metrics_tdx.project' || ( rm -f $(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp; exit 1)
	$(Q)touch $(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp

$(build)/workspaces/data_node.tgz : \
	$(build)/check_workspace_ws-data_node.success

	@echo Archiving workspace data_node >&2
	$(Q)mkdir -p $(build)/workspaces&&$(lb) export-workspace --overwrite data_node $(build)/workspaces/data_node&&$(lb) delete data_node&&(cd $(build)/workspaces/data_node; tar czf ../data_node.tgz *)&&rm -rf $(build)/workspaces/data_node

% : \
	%.sh

.PHONY: archive-ws-data_node
archive-ws-data_node : \
	$(build)/workspaces/data_node.tgz

.PHONY: check
check : \
	all

check-argument-lb_web :

	$(Q)test -e $(lb_web)

check-argument-logicblox :

	$(Q)test -e $(logicblox)

check-argument-measureBlox :

	$(Q)test -e $(measureBlox)

check-lb-libraries :

check-lb-workspaces : \
	check-ws-data_node

.PHONY: check-ws-data_node
check-ws-data_node : \
	$(build)/check_workspace_ws-data_node.success

clean : \
	clean-workspaces

	rm -rf $(build)/sepcomp/dims
	rm -rf $(build)/sepcomp/metrics
	rm -rf $(build)/sepcomp/dims_tdx
	rm -rf $(build)/sepcomp/metrics_tdx
	rm -rf $(build)/sepcomp/data_node
	rm -rf $(build)/sepcomp/measure_dims
	rm -rf $(build)/sepcomp/measure_metrics
	rm -f $(build)/workspaces/data_node.tgz

clean-workspaces :

	@echo Cleaning up >&2
	$(Q)-if [ -e $(build)/check_workspace_ws-data_node.success ]; then lb delete --force data_node;fi
	$(Q)rm -f $(build)/check_workspace_ws-data_node.success

.PHONY: data_node
data_node : \
	$(build)/sepcomp/data_node/LB_SUMMARY.lbp

.PHONY: dims
dims : \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp

.PHONY: dims_tdx
dims_tdx : \
	$(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp

dist : \
	dist_files

	@echo Creating dist tarball >&2
	$(Q)tar zcvf $(package_name).tar.gz $(package_name)
	$(Q)rm -rf $(package_name)

dist_dir : \
	src/core/dims.project \
	src/core/dims/calendar.logic \
	src/core/dims/location.logic \
	src/core/dims/product.logic \
	src/core/metrics.project \
	src/core/metrics/sales.logic \
	src/core/metrics/forecast.logic \
	src/services/tdx_services/dims_tdx.project \
	src/services/tdx_services/dims_tdx/calendar.logic \
	src/services/tdx_services/dims_tdx/location.logic \
	src/services/tdx_services/dims_tdx/product.logic \
	src/services/tdx_services/metrics_tdx.project \
	src/services/tdx_services/metrics_tdx/sales.logic \
	src/services/tdx_services/metrics_tdx/forecast.logic \
	src/data_node.project \
	src/services/measure_dims.project \
	src/services/measure_dims/calendar.logic \
	src/services/measure_dims/location.logic \
	src/services/measure_dims/product.logic \
	src/services/measure_metrics.project \
	src/services/measure_metrics/metrics.logic \
	config.py

	@echo Creating dist dir >&2
	$(Q)rm -rf $(package_name)
	$(Q)mkdir -p $(package_name)

dist_files : \
	dist_dir

	@echo Copying files to dist dir >&2
	$(Q)mkdir -p $(package_name)/`dirname src/core/dims.project`
	$(Q)cp -f src/core/dims.project $(package_name)/`dirname src/core/dims.project`
	$(Q)mkdir -p $(package_name)/`dirname src/core/dims/calendar.logic`
	$(Q)cp -f src/core/dims/calendar.logic $(package_name)/`dirname src/core/dims/calendar.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/core/dims/location.logic`
	$(Q)cp -f src/core/dims/location.logic $(package_name)/`dirname src/core/dims/location.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/core/dims/product.logic`
	$(Q)cp -f src/core/dims/product.logic $(package_name)/`dirname src/core/dims/product.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/core/metrics.project`
	$(Q)cp -f src/core/metrics.project $(package_name)/`dirname src/core/metrics.project`
	$(Q)mkdir -p $(package_name)/`dirname src/core/metrics/sales.logic`
	$(Q)cp -f src/core/metrics/sales.logic $(package_name)/`dirname src/core/metrics/sales.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/core/metrics/forecast.logic`
	$(Q)cp -f src/core/metrics/forecast.logic $(package_name)/`dirname src/core/metrics/forecast.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/dims_tdx.project`
	$(Q)cp -f src/services/tdx_services/dims_tdx.project $(package_name)/`dirname src/services/tdx_services/dims_tdx.project`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/dims_tdx/calendar.logic`
	$(Q)cp -f src/services/tdx_services/dims_tdx/calendar.logic $(package_name)/`dirname src/services/tdx_services/dims_tdx/calendar.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/dims_tdx/location.logic`
	$(Q)cp -f src/services/tdx_services/dims_tdx/location.logic $(package_name)/`dirname src/services/tdx_services/dims_tdx/location.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/dims_tdx/product.logic`
	$(Q)cp -f src/services/tdx_services/dims_tdx/product.logic $(package_name)/`dirname src/services/tdx_services/dims_tdx/product.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/metrics_tdx.project`
	$(Q)cp -f src/services/tdx_services/metrics_tdx.project $(package_name)/`dirname src/services/tdx_services/metrics_tdx.project`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/metrics_tdx/sales.logic`
	$(Q)cp -f src/services/tdx_services/metrics_tdx/sales.logic $(package_name)/`dirname src/services/tdx_services/metrics_tdx/sales.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/tdx_services/metrics_tdx/forecast.logic`
	$(Q)cp -f src/services/tdx_services/metrics_tdx/forecast.logic $(package_name)/`dirname src/services/tdx_services/metrics_tdx/forecast.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/data_node.project`
	$(Q)cp -f src/data_node.project $(package_name)/`dirname src/data_node.project`
	$(Q)mkdir -p $(package_name)/`dirname src/services/measure_dims.project`
	$(Q)cp -f src/services/measure_dims.project $(package_name)/`dirname src/services/measure_dims.project`
	$(Q)mkdir -p $(package_name)/`dirname src/services/measure_dims/calendar.logic`
	$(Q)cp -f src/services/measure_dims/calendar.logic $(package_name)/`dirname src/services/measure_dims/calendar.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/measure_dims/location.logic`
	$(Q)cp -f src/services/measure_dims/location.logic $(package_name)/`dirname src/services/measure_dims/location.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/measure_dims/product.logic`
	$(Q)cp -f src/services/measure_dims/product.logic $(package_name)/`dirname src/services/measure_dims/product.logic`
	$(Q)mkdir -p $(package_name)/`dirname src/services/measure_metrics.project`
	$(Q)cp -f src/services/measure_metrics.project $(package_name)/`dirname src/services/measure_metrics.project`
	$(Q)mkdir -p $(package_name)/`dirname src/services/measure_metrics/metrics.logic`
	$(Q)cp -f src/services/measure_metrics/metrics.logic $(package_name)/`dirname src/services/measure_metrics/metrics.logic`
	$(Q)mkdir -p $(package_name)/`dirname config.py`
	$(Q)cp -f config.py $(package_name)/`dirname config.py`

distcheck : \
	dist

	@echo Testing dist tarball >&2
	$(Q)rm -rf $(build)/distcheck
	$(Q)rm -rf $(build)/$(prefix)
	$(Q)mkdir -p $(build)/distcheck
	$(Q)cd $(build)/distcheck && tar zxf ../../$(package_name).tar.gz
	$(Q)cd $(build)/distcheck/$(package_name) &&lb config --prefix=$(build)/$(prefix)
	$(Q)cd $(build)/distcheck/$(package_name) && $(MAKE)
	$(Q)cd $(build)/distcheck/$(package_name) && $(MAKE) install

install : \
	$(build)/workspaces \
	$(build)/sepcomp/dims \
	$(build)/sepcomp/metrics \
	$(build)/sepcomp/dims_tdx \
	$(build)/sepcomp/metrics_tdx \
	$(build)/sepcomp/data_node \
	$(build)/sepcomp/measure_dims \
	$(build)/sepcomp/measure_metrics

	@echo Installing files into $(prefix) >&2
	$(Q)mkdir -p $(prefix)/workspaces && cp -fR $(build)/workspaces/* $(prefix)/workspaces || echo "Skipping installation of empty directory '$(build)/workspaces'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/dims && cp -fR $(build)/sepcomp/dims/* $(prefix)/share/$(package_basename)/dims || echo "Skipping installation of empty directory '$(build)/sepcomp/dims'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/metrics && cp -fR $(build)/sepcomp/metrics/* $(prefix)/share/$(package_basename)/metrics || echo "Skipping installation of empty directory '$(build)/sepcomp/metrics'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/dims_tdx && cp -fR $(build)/sepcomp/dims_tdx/* $(prefix)/share/$(package_basename)/dims_tdx || echo "Skipping installation of empty directory '$(build)/sepcomp/dims_tdx'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/metrics_tdx && cp -fR $(build)/sepcomp/metrics_tdx/* $(prefix)/share/$(package_basename)/metrics_tdx || echo "Skipping installation of empty directory '$(build)/sepcomp/metrics_tdx'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/data_node && cp -fR $(build)/sepcomp/data_node/* $(prefix)/share/$(package_basename)/data_node || echo "Skipping installation of empty directory '$(build)/sepcomp/data_node'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/measure_dims && cp -fR $(build)/sepcomp/measure_dims/* $(prefix)/share/$(package_basename)/measure_dims || echo "Skipping installation of empty directory '$(build)/sepcomp/measure_dims'"  
	$(Q)mkdir -p $(prefix)/share/$(package_basename)/measure_metrics && cp -fR $(build)/sepcomp/measure_metrics/* $(prefix)/share/$(package_basename)/measure_metrics || echo "Skipping installation of empty directory '$(build)/sepcomp/measure_metrics'"  

lb-libraries : \
	$(build)/sepcomp/dims/LB_SUMMARY.lbp \
	$(build)/sepcomp/metrics/LB_SUMMARY.lbp \
	$(build)/sepcomp/dims_tdx/LB_SUMMARY.lbp \
	$(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp \
	$(build)/sepcomp/data_node/LB_SUMMARY.lbp \
	$(build)/sepcomp/measure_dims/LB_SUMMARY.lbp \
	$(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp

.PHONY: measure_dims
measure_dims : \
	$(build)/sepcomp/measure_dims/LB_SUMMARY.lbp

.PHONY: measure_metrics
measure_metrics : \
	$(build)/sepcomp/measure_metrics/LB_SUMMARY.lbp

.PHONY: metrics
metrics : \
	$(build)/sepcomp/metrics/LB_SUMMARY.lbp

.PHONY: metrics_tdx
metrics_tdx : \
	$(build)/sepcomp/metrics_tdx/LB_SUMMARY.lbp

.PHONY: workspaces
workspaces : \
	archive-ws-data_node

